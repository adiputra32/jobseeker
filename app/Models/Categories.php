<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Goods;

class Categories extends Model
{
    use HasFactory;

    protected $table = "tb_categories";
    public $timestamp = false;
    protected $primaryKey = "id_categories";
    protected $fillable = [
        "category",
    ];

    public function goods(){
    	return $this->hasMany(Goods::class, 'id_categories');
    }
}
