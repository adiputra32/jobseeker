<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Categories;

class Goods extends Model
{
    use HasFactory;

    protected $table = "tb_goods";
    public $timestamp = false;
    protected $primaryKey = "id_goods";
    protected $fillable = [
        "goods",
        "id_categories",
        "price",
        "discount_price",
    ];

    public function categories(){
    	return $this->belongsTo(Categories::class, 'id_categories');
    }
}
