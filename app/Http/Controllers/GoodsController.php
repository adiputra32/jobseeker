<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Goods;
use App\Models\Categories;
use Illuminate\Support\Facades\Validator;

class GoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::all();
        $goods = Goods::all();

        return view('goods.goods')->with(compact('categories', 'goods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'goods' => 'required|unique:tb_goods,goods',
        ]);

        if ($validator->fails()) {
            return $result = [
                "status" => false,
                "info" => "Failed! Goods name duplicated.",
            ];
        }

        $price = $request->price;
        $discount_price = 0;
        if ($price >= 40000) {
            $discount_price = $price-($price * 10 / 100);
        }

        try {
            $goods = Goods::insert([
                'goods' => $request->goods,
                'id_categories' => $request->id_categories,
                'price' => $price,
                'discount_price' => $discount_price,
            ]);
        } catch(\Illuminate\Database\QueryException $ex){ 
            return $result = [
                "status" => false,
                "info" => "Failed! Goods not added",
            ];
        }

        if (!$goods) {
            return $result = [
                "status" => false,
                "info" => "Failed! Goods not added",
            ];
        } else {
            $goodCount = Goods::get()->count();
            $newGoods = Goods::select('id_goods', 'goods', 'category', 'price', 'discount_price')
                ->join('tb_categories', 'tb_categories.id_categories', 'tb_goods.id_categories')
                ->latest('id_goods')
                ->first();

            return $result = [
                "status" => true,
                "info" => "Goods successfully added!",
                "number" => $goodCount,
                "data" => $newGoods,
            ]; 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Categories::all();
        $goods = Goods::select('id_goods', 'goods', 'category', 'price', 'discount_price', 'tb_goods.id_categories')
            ->join('tb_categories', 'tb_categories.id_categories', 'tb_goods.id_categories')
            ->where('id_goods', $id)
            ->first();

        return $result = [
            "goods" => $goods,
            "categories" => $categories,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'goods' => 'required|unique:tb_goods,goods',
        ]);

        if ($validator->fails()) {
            return $result = [
                "status" => false,
                "info" => "Failed! Goods name duplicated.",
            ];
        }

        $price = $request->price;
        $discount_price = 0;
        if ($price >= 40000) {
            $discount_price = $price-($price * 10 / 100);
        }

        try {
            $goods = Goods::where('id_goods', $id)->update([
                'goods' => $request->goods,
                'id_categories' => $request->id_categories,
                'price' => $price,
                'discount_price' => $discount_price,
            ]);
        } catch(\Illuminate\Database\QueryException $ex){ 
            return $result = [
                "status" => false,
                "info" => "Failed! Goods not updated",
            ];
        }

        if ($goods > 0) {
            $goodCount = Goods::get()->count();
            $newGoods = Goods::select('id_goods', 'goods', 'category', 'price', 'discount_price')
                ->join('tb_categories', 'tb_categories.id_categories', 'tb_goods.id_categories')
                ->where('id_goods', $id)
                ->first();
                
            return $result = [
                "status" => true,
                "info" => "Goods successfully updated!",
                "number" => $goodCount,
                "data" => $newGoods,
            ]; 
        } else {
            return $result = [
                "status" => false,
                "info" => "Failed! Goods not updated",
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $goods = Goods::find($id)->delete($id);
        } catch(\Illuminate\Database\QueryException $ex){ 
            return $result = [
                "status" => false,
                "info" => "Failed! Goods not deleted",
            ];
        }


        if ($goods) {
            $newGoods = Goods::select('id_goods', 'goods', 'category', 'price', 'discount_price')
                ->join('tb_categories', 'tb_categories.id_categories', 'tb_goods.id_categories')
                ->get();
                
            return $result = [
                "status" => true,
                "info" => "Goods has been deleted!",
                "goods" => $newGoods,
            ]; 
        } else {
            return $result = [
                "status" => false,
                "info" => "Failed! Goods not deleted",
            ];
        }
    }

    public function requestData()
    {
        $goods = Goods::select('id_goods', 'goods', 'category', 'price', 'discount_price')
            ->join('tb_categories', 'tb_categories.id_categories', 'tb_goods.id_categories')
            ->get();

        return $goods;
    }
}
