<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <title>Back-End Case Test 1</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" >
    <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" >
    <link href="https://cdn.datatables.net/fixedheader/3.2.3/css/fixedHeader.dataTables.min.css" rel="stylesheet" type="text/css" >

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
    {{-- Add Goods Form --}}
    <div class="card" style="width: 80vw; margin: 1rem;">
        <div class="card-header text-center">
            Add New Goods
        </div>
        <div class="card-body">
            <form id="addGoods">
                <div class="mb-3">
                    <label for="goods" class="form-label">Goods</label>
                    <input type="text" class="form-control" id="goods" name="goods" placeholder="Goods (ex: Headset)" required>
                </div>
                <div class="mb-3">
                    <label for="id_categories" class="form-label">Category</label>
                    <div style="width: 100%;">
                        <div style="display: inline-flex;">
                            <select class="form-select" id="id_categories" name="id_categories" aria-label="Default select example" required>
                                <option value="" disabled selected>-- Choose one --</option>
                            </select>    
                        </div>
                        <div style="display: inline-flex;">
                            <a class="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#addCategoryModal">Add Category</a>
                        </div>
                    </div>
                    
                </div>
                <div class="mb-3">
                    <label for="price" class="form-label">Price</label>
                    <input type="number" min="0" class="form-control" id="price" name="price" placeholder="Price" required>
                </div>
                <div class="mb-3">
                    <button class="btn btn-primary" form="addGoods" type="submit">Add Goods</button>
                </div>
            </form>
        </div>
    </div>

    {{-- DataTable Goods --}}
    <div class="card" style="width: 80vw; margin: 1rem;">
        <div class="card-header text-center">
            Goods List
        </div>
        <div class="card-body" style="overflow: auto;">
            <table id="goodsDataTable" class="table" style="width: 100%; margin: 1rem; overflow: auto;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Goods</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Add Category Modal -->
    <div class="modal fade" id="addCategoryModal" tabindex="-1" aria-labelledby="addCategoryModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addCategoryModalLabel">Add Category</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="addCategories">
                        <div class="mb-3">
                            <label for="categories" class="form-label">Categories</label>
                            <input type="text" class="form-control" id="categories" name="category" placeholder="Category (ex: Retail)" required>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" form="addCategories" class="btn btn-primary">Add Category</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Goods Modal -->
    <div class="modal fade" id="editGoodsModal" tabindex="-1" aria-labelledby="editGoodsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editGoodsModalLabel">Edit Goods</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="editGoods">
                        <div class="mb-3">
                            <label for="goods" class="form-label">Goods</label>
                            <input type="text" class="form-control" id="edit_goods" name="goods" placeholder="Goods (ex: Headset)" required>
                        </div>
                        <div class="mb-3">
                            <label for="id_categories" class="form-label">Category</label>
                            <div style="width: 100%;">
                                <div style="display: inline-flex;">
                                    <select class="form-select" id="edit_id_categories" name="id_categories" aria-label="Default select example" required>
                                        <option disabled selected>-- Choose one --</option>
                                    </select>    
                                </div>
                                <div style="display: inline-flex;">
                                    <a class="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#addCategoryModal">Add Category</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="mb-3">
                            <label for="price" class="form-label">Price</label>
                            <input type="number" min="0" class="form-control" id="edit_price" name="price" placeholder="Price" required>
                        </div>
                        <input type="hidden" id="edit_id_goods" name="id_goods" required>
                        <input type="hidden" name="_method" value="PATCH" required>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" form="editGoods" class="btn btn-primary">Save Changes</button>
                </div>
            </div>
        </div>
    </div>

    {{-- scripts --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.2.3/js/dataTables.fixedHeader.min.js"></script>

    <script>
        var editedGoodsRow = "";

        $(document).ready(function() {
            // goods DataTable - clone header for column filter
            $('#goodsDataTable thead tr')
                .clone(true)
                .addClass('filters')
                .appendTo('#goodsDataTable thead');
        
            // goods DataTable
            var goodsTable = $('#goodsDataTable').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                initComplete: function () {
                    var api = this.api();
        
                    // For each column
                    api.columns().eq(0).each(function (colIdx) {
                        // Set the header cell to contain the input element
                        var cell = $('.filters th').eq(
                            $(api.column(colIdx).header()).index()
                        );
                        var title = $(cell).text();
                        $(cell).html('<input type="text" placeholder="' + title + '" />');
    
                        // On every keypress in this input
                        $('input', $('.filters th').eq($(api.column(colIdx).header()).index())).off('keyup change').on('change', function (e) {
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();

                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api.column(colIdx).search(
                                this.value != ''
                                    ? regexr.replace('{search}', '(((' + this.value + ')))')
                                    : '',
                                this.value != '',
                                this.value == ''
                            ).draw();
                        }).on('keyup', function (e) {
                            e.stopPropagation();

                            $(this).trigger('change');
                            $(this).focus()[0].setSelectionRange(cursorPosition, cursorPosition);
                        });
                    });
                },
            });

            // get goods
            $.ajax({
                type: 'get',
                url: 'goods/request/all',
                success: function (msg) {
                    console.log(msg);
                    $.each( msg, function( key, data ) {
                        var order = key+1;
                        var row = key;
                        var price = 0;
                        var rowAction = "<a role='button' onclick='editGoods(" + data.id_goods + ", " + row + ")'>Edit</a>" +
                            "<span> | </span>" +
                            "<a role='button' onclick='deleteGoods(" + data.id_goods + ")'>Delete</a> ";
                        
                        if (parseInt(data.price) >= 40000) {
                            price = (parseInt(data.discount_price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + " <s>" + (parseInt(data.price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + "</s>";
                        } else {
                            price = (parseInt(data.price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                        }

                        // goods DataTable - append data
                        goodsTable.row.add([order, data.goods, data.category, price, rowAction]).draw(false);
                    });
                }
            });

            // add goods AJAX
            $('#addGoods').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: 'goods',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: $('#addGoods').serialize(),
                    success: function (msg) {
                        console.log(msg);
                        if (msg.status) {
                            $('#addGoods').trigger('reset');

                            var price = 0;
                            var row = msg.data.number+1;
                            var rowAction = "<a role='button' onclick='editGoods(" + msg.data.id_goods + ", " + row + ")'>Edit</a>" +
                                "<span> | </span>" +
                                "<a role='button' onclick='deleteGoods(" + msg.data.id_goods + ")'>Delete</a> ";
                        
                            if (parseInt(msg.data.price) >= 40000) {
                                price = (msg.parseInt(data.discount_price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + " <s>" + (msg.parseInt(data.price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + "</s>";
                            } else {
                                price = (msg.parseInt(data.price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            }


                            // goods DataTable - append data
                            goodsTable.row.add([msg.number, msg.data.goods, msg.data.category, price, rowAction]).draw(false);
                        } 

                        alert(msg.info);   
                    }
                });
            });

            // edit goods AJAX - get data
            $.editGoods = function(id) {
                $.ajax({
                    type: 'get',
                    url: 'goods/' + id + '/edit',
                    success: function (msg) {
                        $.each( msg.categories, function( key, data ) {
                            $('#edit_id_categories').append($('<option>', { 
                                value: data.id_categories,
                                text : data.category 
                            }));
                        });

                        $('#edit_id_goods').val(msg.goods.id_goods);
                        $('#edit_goods').val(msg.goods.goods);
                        $('#edit_price').val(msg.goods.price);
                        $('#edit_id_categories').val(msg.goods.id_categories)
                    }
                });
            };

            // edit goods modal on hidden
            $('#editGoodsModal').on('hidden.bs.modal', function () {
                $('#editGoods').trigger('reset');
                $('#edit_id_categories').empty().append('<option value="" disabled selected>-- Choose one --</option>');
            })

            // edit goods AJAX - post data
            $('#editGoods').on('submit', function (e) {
                e.preventDefault();
                var id = $('#edit_id_goods').val();
                $.ajax({
                    type: 'post',
                    url: 'goods/' + id,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: $('#editGoods').serialize(),
                    success: function (msg) {
                        if (msg.status) {
                            var price = 0;
                            var rowAction = "<a role='button' onclick='editGoods(" + msg.data.id_goods + ", " + editedGoodsRow + ")'>Edit</a>" +
                                "<span> | </span>" +
                                "<a role='button' onclick='deleteGoods(" + msg.data.id_goods + ")'>Delete</a> ";     

                            if (parseInt(msg.data.price) >= 40000) {
                                price = (parseInt(msg.data.discount_price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + " <s>" + (parseInt(msg.data.price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + "</s>";
                            } else {
                                price = (parseInt(msg.data.price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            }
                                
                            var oldGoods = [ 
                                editedGoodsRow+1, 
                                msg.data.goods, 
                                msg.data.category,
                                price,
                                rowAction,
                            ];
                            goodsTable.row(editedGoodsRow).data(oldGoods).draw();

                            $('#editGoodsModal').modal('toggle');
                        } 

                        alert(msg.info);   
                    }
                });
            });

            // get categories
            $.ajax({
                type: 'get',
                url: 'categories',
                success: function (msg) {
                    $.each( msg, function( key, data ) {
                        $('#id_categories').append($('<option>', { 
                            value: data.id_categories,
                            text : data.category 
                        }));
                    });
                }
            });

            // add categories AJAX
            $('#addCategories').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: 'categories',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: $('#addCategories').serialize(),
                    success: function (msg) {
                        if (msg.status) {
                            $('#id_categories').append($('<option>', { 
                                value: msg.value,
                                text : msg.text 
                            }));

                            $('#addCategoryModal').modal('toggle');
                        }

                        alert(msg.info);
                    }
                });
            });

            // add categories modal on hidden
            $('#addCategories').on('hidden.bs.modal', function () {
                $('#addCategories').trigger('reset');
            })

            // delete goods AJAX
            $.deleteGoods = function(id) {
                $.ajax({
                    type: 'delete',
                    url: 'goods/' + id,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (msg) {
                        console.log(msg);
                        if (msg.status) {
                            // clear goods DataTable
                            goodsTable.clear().draw();

                            // reset goods DataTable
                            $.each( msg.goods, function( key, data ) {
                                var order = key+1;
                                var row = key;
                                var price = 0;
                                var rowAction = "<a role='button' onclick='editGoods(" + data.id_goods + ", " + row + ")'>Edit</a>" +
                                    "<span> | </span>" +
                                    "<a role='button' onclick='deleteGoods(" + data.id_goods + ")'>Delete</a> ";
                             
                                if (parseInt(data.price) >= 40000) {
                                    price = (parseInt(data.discount_price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + " <s>" + (parseInt(data.price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + "</s>";
                                } else {
                                    price = (parseInt(data.price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                                }

                                // goods DataTable - append data
                                goodsTable.row.add([order, data.goods, data.category, price, rowAction]).draw(false);
                            });
                        }

                        alert(msg.info);   
                    }
                });
            };
        });

        // call jquery function editGoods
        function editGoods(id, row) {
            $('#editGoodsModal').modal('toggle');
            $.editGoods(id);
            editedGoodsRow = row;
        };

        // call jquery function deleteGoods
        function deleteGoods(id) {
            if (confirm('Are you sure to delete this row?')) {
                $.deleteGoods(id);
            } 
        };
    </script>
</body>
</html>