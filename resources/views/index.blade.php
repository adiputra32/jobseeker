<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Front-End Case Test</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
    <link href='https://fonts.googleapis.com/css?family=Manrope' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>

    <style>
        body {
            position: relative;
            background: #FAFBFF;
        }

        /* navbar */
        .navbar {
            background: linear-gradient(180deg, rgba(0,0,0,0.6278886554621849) 0%, rgba(255,255,255,0) 71%);
        }

        .navbar-cointainer {
            padding-left: 113px;
            padding-right: 113px;
        }

        .logo {
            height: 62px;
            width: 79.82499694824219px;

            /* margin-left: 113px; */
            margin-top: 19px;

            border-radius: 0px;
        }

        .btn-contact {
            height: 36px;
            width: 132px;
            margin-right: 50.5px;

            background: #E4007E;
            border-radius: 8px;

            font-family: 'Manrope';
            font-style: normal;
            font-weight: 700;
            font-size: 14px;
            line-height: 19px;
            text-align: center;

            color: #FFFFFF;
        }

        .btn-language-slash {
            height: 36px;
            bottom: 0;

            font-family: 'Manrope';
            font-style: normal;
            font-weight: normal;
            font-size: 20px;
            text-align: center;
            letter-spacing: 0em;
        }

        .btn-language {
            height: 36px;
            width: auto;

            font-family: 'Manrope';
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            line-height: 19px;
            text-align: center;
            text-decoration-line: underline;
            letter-spacing: 0em;
            background-color: transparent;
            border: none;

            color: #FFFFFF;
        }

        .btn-language.active {
            font-weight: 800;
        }

        /* header */
        header {
            position: relative;
            height: 100vh;
            min-height: 25rem;
            width: 100%;
            overflow: hidden;

            background: url('../assets/images/header.png') no-repeat center center; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        /* header video {
            position: absolute;
            width: auto;
            height: auto;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            z-index: 0;

            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
        } */

        header .header-content {
            position: absolute;
            width: 50%;
            top: 50%;
            left: 50%;
            z-index: 2;

            transform: translateX(-50%) translateY(-50%);
        }

        header .overlay-dark {
            position: absolute;
            height: 100vh;
            width: 100vw;
            top: 0;
            left: 0;
            opacity: 0.5;
            background-color: black;
            z-index: 1;
        }

        .text-header {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 48px;
            line-height: 58px;
            /* or 58px */

            text-align: center;

            color: #FFFFFF;
        }

        .sub-text-header {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 18px;
            line-height: 27px;
            /* or 27px */

            text-align: center;

            color: #FFFFFF;
        }

        .btn-see-our-product {
            height: 60px;
            width: 162px;

            background: #E4007E;
            border-radius: 8px;

            font-family: 'Manrope';
            font-style: normal;
            font-weight: 700;
            font-size: 14px;
            line-height: 30px;
            text-align: center;
            letter-spacing: -0.02em;

            color: #FFFFFF;
        }

        /* products */
        .products {
            position: relative;
            width: 100vw;
            min-height: 100vh;
            padding-top: 100px;
            padding-bottom: 100px;
        }

        .our-products {
            position: relative;
            width: 100%;

            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 48px;
            line-height: 56px;
            text-align: center;
            letter-spacing: -0.03em;

            color: #000000;
        }

        .rectangle-1 {
            position: relative;
            margin: 50px auto auto auto;
            padding: 3rem 2rem 5rem 2rem;
            height: auto;
            width: 80%;

            background: #F0EEFE;
            box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.1);
            border-radius: 20px;
            overflow: auto;
        }

        .rectangle-2 {
            position: relative;
            margin: 10px 20px auto 20px;
            padding: 3rem 1rem 5rem 1rem;
            height: auto;
            width: 48%;

            background: #F0EEFE;
            box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.1);
            border-radius: 20px;
            overflow: auto;
        }

        .container-rectangle-2 {
            position: relative;
            margin: 50px auto auto auto;
            height: auto;
            width: 80%;
        }

        .rectangle-title {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 36px;
            line-height: 44px;

            background: linear-gradient(268.91deg, #050774 -7.14%, #E4007E 96.32%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            background-clip: text;
            text-fill-color: transparent;
        }

        .rectangle-body-1 {
            margin: auto;
            width: 50%
        }

        .rectangle-body-2 {
            margin: auto;
            width: 80%
        }

        .rectangle-brand {
            margin: 3rem auto auto auto;
            width: fit-content;
        }

        .circle-brand-logo {
            width: 73px;
            height: 73px;

            background: #FFFFFF;
            box-shadow: 0px 1.34737px 8.20513px rgba(0, 0, 0, 0.15);
            border-radius: 336.505px;
        }

        .rectangle-brand-logo {
            height: 50px;
            width: 73px;
            margin-top: 10px;
            border-radius: 0px;
        }

        .rectangle-brand-text {
            margin-top: 10px;
            margin-left: 1rem;

            font-family: Inter;
            font-size: 40px;
            font-weight: 600;
            line-height: 48px;
            letter-spacing: 0em;
            text-align: center;


            text-align: center;

            color: #000000;
        }

        .rectangle-text {
            margin-top: 3rem;

            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 18px;
            line-height: 150%;

            text-align: center;

            color: #8B8B8B;
        }

        .rectangle-learn-more {
            margin-top: 4rem;

            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 18px;
            line-height: 135%;
            /* or 24px */

            text-decoration-line: none;

            color: #050774;
        }

        /* feature */
        .feature {
            position: relative;
            width: 100vw;
            min-height: 130vh;
            padding-top: 100px;
            padding-bottom: 100px;

            background: url('../assets/images/Rectangle.png') no-repeat center center fixed; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .feature-container {
            position: relative;
            margin: auto;
            width: 50vw; 
            height: auto;
        }

        .video-wrapper {
            position: relative;
            display: block;
            overflow: hidden;
            width: fit-content;
            height: fit-content;
            border-radius: 50px;
            margin: auto;
            transform: translateZ(0px);
        }

        .feature-content {
            position: relative;
            margin-top: 5rem;
        }

        .feature-about-us {
            width: 40%;
            float: left;
        }

        .feature-about-us-text {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 48px;
            line-height: 120%;
            text-align: left;

            color: #FFFFFF;
        }

        .feature-caption {
            width: 40%;
            float: right;
        }

        .feature-caption-text {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 18px;
            line-height: 150%;
            text-align: left;

            color: #FFFFFF;
        }

        /* culture */
        .culture {
            position: relative;
            width: 100vw;
            min-height: 100vh;
            padding-top: 100px;
            padding-bottom: 50px;
        }

        .culture-container {
            position: relative;
            margin: auto;
            width: 80vw; 
            height: auto;
        }

        .culture-picture {
            height: 560px;
            width: 569px;
            border-radius: 20px;
            float: left;
        }

        .culture-title {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 48px;
            line-height: 120%;
            text-align: left;

            color: #000000;
        }

        .culture-content-wrapper {
            padding: 1rem 1rem 1rem 3rem;
            margin: auto;
            text-align: left;
        }

        .culture-caption {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 18px;
            line-height: 150%;
            text-align: left;

            color: #8B8B8B;
        }

        .btn-culture-join-us {
            position: absolute;
            height: 60px;
            width: 162px;
            float: left;
            border-radius: 7px;

            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 18px;
            line-height: 30px;

            text-align: center;
            letter-spacing: -0.02em;

            color: #FFFFFF;

            background: #050774;
        }

        /* team */
        .team {
            position: relative;
            width: 100vw;
            min-height: 100vh;
            padding-top: 50px;
            padding-bottom: 50px;
        }

        .team-container {
            position: relative;
            margin: auto;
            width: 80vw; 
            height: auto;
        }

        .team-title {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 48px;
            line-height: 58px;
            text-align: left;

            color: #000000;
        }

        .team-caption {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 18px;
            line-height: 23px;
            text-align: left;

            letter-spacing: 0.3px;

            color: #8B8B8B;

        }

        .team-sub-title {
            margin-top: 20px;

            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 20px;
            line-height: 24px;
            text-transform: uppercase;
            text-align: left;

            color: #000000;
        }

        .position-location {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 20px;
            line-height: 40px;
            /* or 200% */

            letter-spacing: 0.3px;
            text-decoration-line: none;

            color: #8B8B8B;
        }

        .position-location.active {
            color: #000000 !important;
            text-decoration-line: underline !important;
        }

        .position-location-list {
            list-style-type: none;
            text-align: left;;
        }

        .position-card {
            background: #FFFFFF;
            box-shadow: 0px 0px 17.5358px rgba(0, 0, 0, 0.1);
            border-radius: 16.5449px;
            padding: 2rem;
            margin-bottom: 1rem;
        }

        .position-card-title {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 30px;
            line-height: 45px;
            font-family: Inter;
            letter-spacing: 0em;
            text-align: left;


            color: #18181B;
        }

        .position-card-location {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 20px;
            line-height: 45px;
            letter-spacing: 0em;
            text-align: left;

            color: #20A6FC;
        }

        .position-card-content, tr, td {
            vertical-align: top;
            text-align: left;
            border: none;
        }

        .btn-position-card-apply {
            height: 44.6711311340332px;
            width: 165.44862365722656px;
            left: 1268.3729858398438px;
            top: 4774.510498046875px;
            border-radius: 7.014301300048828px;

            font-family: Manrope;
            font-size: 14px;
            font-weight: 600;
            line-height: 21px;
            letter-spacing: 0em;
            text-align: center;

            color: #FFFFFF;

            background: #050774;
            border-radius: 7.0143px;
        }

        .position-card-more {
            margin-top: 1rem;

            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 18px;
            line-height: 135%;
            text-align: center;
            /* or 24px */

            text-decoration-line: none;

            color: #050774;
        }

        /* directors */
        .directors {
            position: relative;
            width: 100vw;
            min-height: 50vh;
            padding-top: 50px;
            padding-bottom: 150px;
        }

        .directors-container {
            position: relative;
            margin: auto;
            width: 50vw; 
            height: auto;
        }

        .directors-title {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 56px;
            line-height: 67px;
            letter-spacing: 0px;
            text-align: center;

            color: #000000;
        }

        .directors-image {
            width: 100%;
            height: 200px;
        }

        .directors-name {
            width: 125px;
            height: 22px;

            font-family: 'Inter';
            font-style: normal;
            font-weight: 800;
            font-size: 18px;
            line-height: 22px;
            text-align: center;

            color: #000000;
        }

        .directors-position {
            width: 207px;
            height: 23px;

            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 18px;
            line-height: 23px;
            text-align: center;
            letter-spacing: 0.3px;

            color: #8B8B8B;
        }

        /* footer */
        .footer {
            position: relative;
            width: 100vw;
            min-height: 50vh;
            padding-top: 50px;
            padding-bottom: 100px;

            background: #202020;
        }

        .footer-container {
            position: relative;
            margin: auto;
            width: 80vw; 
            height: auto;
        }

        .footer-logo {
            margin-bottom: 2rem;
        }

        .footer-address {
            font-family: 'Inter';
            font-style: normal;
            font-size: 14px;
            line-height: 21px;

            color: #8B8B8B;
        }

        .footer-email {
            text-decoration-line: none;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 14px;
            line-height: 21px;

            color: #FFFFFF;
        }

        .footer-socmed {
            text-decoration-line: none;
            color: #FFFFFF;
        }

        .footer-app-url {
            text-decoration-line: none;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 14px;
            line-height: 21px;

            color: #FFFFFF;
        }

        .footer-app-url.disabled {
            pointer-events: none;
            cursor: default;
            
            color: #8B8B8B;
        }

        .footer-app-coming-soon {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 900;
            font-size: 10px;
            line-height: 15px;

            color: #E4007E;
        }

        .footer-divider {
            border: none;
            height: 1px;
            margin: 2rem 0 2rem 0;

            color: #FFFFFF; /* old IE */
        }

        .copyright {
            font-family: 'Roboto';
            font-style: normal;
            font-weight: 400;
            font-size: 14px;
            line-height: 21px;

            color: #FFFFFF;
        }

        .footer-link {
            font-family: 'Roboto';
            font-style: normal;
            font-weight: 400;
            font-size: 14px;
            line-height: 21px;
            text-decoration-line: underline;
            padding: 0 0 0 3rem;

            color: #FFFFFF;
        }
    </style>
</head>
<body>
    
    {{-- Navigation --}}
    <nav class="navbar fixed-top navbar-light" >
        <div class="container-fluid navbar-cointainer">
            <a class="navbar-brand" href="#">
                <img class="logo" src="{{ asset('/assets/images/jobseeker_white.png') }}" alt="" width="30" height="24">
            </a>

            <form class="nav-button-group d-flex">
                <button class="btn btn-contact" type="button">Contact Us</button>
                <div class="d-flex text-white">
                    <button class="btn-language active" type="button"> English</button>
                    <span class="btn-language-slash"> / </span>
                    <button class="btn-language" type="button"> Indonesia</button>
                </div>

              </form>
        </div>
    </nav>

    {{-- Section Header --}}
    <header class="header">
        <div class="header-content">
            <div class="h-100 text-center align-items-center">
                <h1 class="text-header">Connecting people with opportunities</h1> <br>
                <p class="sub-text-header">Kami percaya setiap orang berhak memiliki kesempatan untuk berkarya. Jobseeker Company hadir, untuk membantu menghubungkan mereka dengan kesempatan berkembang demi kehidupan yang lebih baik.</p> <br>
                <button class="btn btn-see-our-product" type="button">See Our Product</button>
            </div>
        </div>

        <div class="overlay-dark"></div>
    </header>

    {{-- Section Products --}}
    <section class="products">
        <div class="text-center align-items-center">
            <h2 class="our-products">Our Products</h2>

            {{-- product for candidate --}}
            <div class="rectangle-1">
                <h3 class="rectangle-title">For Candidate</h3>
                <div class="rectangle-body-1">
                    <div class="rectangle-brand d-flex">
                        <div class="circle-brand-logo">
                            <img class="rectangle-brand-logo" src="{{ asset('/assets/images/JSWAPP1.png') }}" alt="">
                        </div>
                        <span class="rectangle-brand-text">jobseeker.app</span>
                    </div>
                    <p class="rectangle-text">Social recruitment platform untuk para pencari kerja, terutama untuk non white-collar worker yang jumlahnya 4-5x lebih banyak daripada angkatan kerja white collar. Jobseker.App memungkinkan pencari kerja untuk mencari kerja secara lebih mudah, cepat, dan sesuai dengan lokasi mereka.</p>
                    <a class="rectangle-learn-more" href="#">
                        Learn more <i class="fa-solid fa-arrow-right"></i>
                    </a>
                </div>
            </div>

            {{-- product for employer --}}
            <div class="rectangle-1">
                <h3 class="rectangle-title">For Employer</h3>
                <div class="rectangle-body-1">
                    <div class="rectangle-brand d-flex">
                        <div class="circle-brand-logo">
                            <img class="rectangle-brand-logo" src="{{ asset('/assets/images/JSWAPP1.png') }}" alt="">
                        </div>
                        <span class="rectangle-brand-text">jobseeker.partners</span>
                    </div>
                    <p class="rectangle-text">Indonesia memasuki era keemasan bonus demografi yang diprediksi mencapai puncaknya pada 2030. Manfaatkan peluang ini untuk mendapatkan tenaga kerja terbaik dengan mudah, cepat, terdekat dari lokasi usaha.</p>
                    <a class="rectangle-learn-more" href="#">
                        Learn more <i class="fa-solid fa-arrow-right"></i>
                    </a>
                </div>
            </div>

            <div class="d-flex container-rectangle-2">
                {{-- jobseeker software --}}
                <div class="rectangle-2">
                    <div class="rectangle-body-2">
                        <div class="rectangle-brand d-flex">
                            <div class="circle-brand-logo">
                                <img class="rectangle-brand-logo" src="{{ asset('/assets/images/JSWAPP1.png') }}" alt="">
                            </div>
                            <span class="rectangle-brand-text">jobseeker.software</span>
                        </div>
                        <p class="rectangle-text">99% perusahaan Fortune 500 menggunakan
                            Applicant Tracking System (ATS) dalam strategi rekrutmen mereka. Software ATS membantu anda melakukan proses rekrutmen dari awal hingga akhir secara efektif dan efisien.</p>
                        <a class="rectangle-learn-more" href="#">
                            Learn more <i class="fa-solid fa-arrow-right"></i>
                        </a>
                    </div>
                </div>

                {{-- jobseeker service --}}
                <div class="rectangle-2">
                    <div class="rectangle-body-2">
                        <div class="rectangle-brand d-flex">
                            <div class="circle-brand-logo">
                                <img class="rectangle-brand-logo" src="{{ asset('/assets/images/JSWAPP1.png') }}" alt="">
                            </div>
                            <span class="rectangle-brand-text">jobseeker.services</span>
                        </div>
                        <p class="rectangle-text">jobseekers.services memiliki akses ke jutaan kandidat dan talent pool untuk proses rekruitmen karyawan yang lebih cepat dengan harga yang lebih terjangkau.</p>
                        <a class="rectangle-learn-more" href="#">
                            Learn more <i class="fa-solid fa-arrow-right"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    {{-- Section Feature --}}
    <section class="feature">
        <div class="feature-container text-center align-items-center">
            <div class="video-wrapper">
                <iframe style="width: 50vw; height: 30vw;" src="https://www.youtube.com/embed/DgqDDI2R9m4?autoplay=0&autohide=1&controls=1&enablejsapi=1" title="YouTube video player" 
                frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>    
            </div>

            <div class="feature-content">
                <div class="feature-about-us">
                    <h2 class="feature-about-us-text">About Us</h2>
                </div>
                <div class="feature-caption">
                    <p class="feature-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.</p>
                </div>
            </div>
            
        </div>
    </section>

    {{-- Section Our Culture --}}
    <section class="culture">
        <div class="culture-container text-center align-items-center">
            <div class="row">
                <div class="col-6">
                    <img class="culture-picture" src="{{ asset('/assets/images/IMG_4675.png') }}" alt="">
                </div>
                <div class="col-6 culture-content-wrapper">
                    <h3 class="culture-title">Our Culture</h3>
                    <p class="culture-caption">Though we move at a fast pace, and it might seem that we don’t have time to sit and explain things, every employee at Jobseeker Company loves sharing, helping and lifting others. We have unique set of values on clear goals that define success.</p>
                    <button class="btn-culture-join-us">Join Us</button>
                </div>
            </div>
        </div>
    </section>

    {{-- Section Join Our Team --}}
    <section class="team">
        <div class="team-container text-center align-items-center">
            <div class="row">
                <div class="col-12">
                    <h3 class="team-title">Join Our Team</h3>
                </div>
                
                <div class="col-8">
                    <p class="team-caption">
                        We're building the first social recruitment platform in Indonesia. Jobseeker Company is perfect for those who:

                        <ul class="team-caption">
                            <li>Want to work in a fast-paced startup</li>
                            <li>Love having a sense of ownership to your projects</li>
                            <li>Want to take part in building the next big startup in Indonesia</li>
                        </ul>
                    </p>
                </div>
            </div>

            {{-- positions --}}
            <div class="row">
                <div class="col-12">
                    <h3 class="team-sub-title">CURRENT POSITIONS</h3> <br>
                </div>

                <div class="row">
                    <div class="col-3">
                        <ul>
                            <li class="position-location-list"><a class="position-location active" href="#">All Locations</a></li>
                            <li class="position-location-list"><a class="position-location" href="#">Jakarta</a></li>
                            <li class="position-location-list"><a class="position-location" href="#">Malang</a></li>
                        </ul>
                    </div>
                    <div class="col-9">
                        <div class="position-card">
                            <div class="row">
                                <div class="col-5">
                                    <h5 class="position-card-title">Marketing Manager</h5>

                                    <table class="table position-card-content">
                                        <tr>
                                            <td>Reuirements</td>
                                            <td>
                                                Bachelor Degree <br>
                                                Min. 5 years of experience
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Sallary</td>
                                            <td>IDR 10 - 15 Millions</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 2rem;">10 mins ago</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-3">
                                    <span class="position-card-location">
                                        <i class="fa-solid fa-location-dot"></i> Jakarta
                                    </span>
                                </div>
                                <div class="col-4">
                                    <button class="btn-position-card-apply">Apply</button>
                                </div>
                            </div>
                        </div>

                        <div class="position-card">
                            <div class="row">
                                <div class="col-5">
                                    <h5 class="position-card-title">Marketing Manager</h5>

                                    <table class="table position-card-content">
                                        <tr>
                                            <td>Reuirements</td>
                                            <td>
                                                Bachelor Degree <br>
                                                Min. 5 years of experience
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Sallary</td>
                                            <td>IDR 10 - 15 Millions</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 2rem;">10 mins ago</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-3">
                                    <span class="position-card-location">
                                        <i class="fa-solid fa-location-dot"></i> Jakarta
                                    </span>
                                </div>
                                <div class="col-4">
                                    <button class="btn-position-card-apply">Apply</button>
                                </div>
                            </div>
                        </div>

                        <div class="position-card">
                            <div class="row">
                                <div class="col-5">
                                    <h5 class="position-card-title">Marketing Manager</h5>

                                    <table class="table position-card-content">
                                        <tr>
                                            <td>Reuirements</td>
                                            <td>
                                                Bachelor Degree <br>
                                                Min. 5 years of experience
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Sallary</td>
                                            <td>IDR 10 - 15 Millions</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 2rem;">10 mins ago</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-3">
                                    <span class="position-card-location">
                                        <i class="fa-solid fa-location-dot"></i> Jakarta
                                    </span>
                                </div>
                                <div class="col-4">
                                    <button class="btn-position-card-apply">Apply</button>
                                </div>
                            </div>
                        </div>

                        <a class="position-card-more" href="#">
                            See all vacancies <i class="fa-solid fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Section Board of Directors --}}
    <section class="directors">
        <div class="directors-container text-center align-items-center">
            <div class="row">
                <div class="col-12">
                    <h3 class="directors-title">Board of Directors</h3>
                </div>
            </div>

            <br><br>

            <div class="row">
                <div class="col-4 directors-wrapper">
                    <img class="directors-image" src="{{ asset('/assets/images/bapak 1.svg') }}" alt=""> <br>
                    <span class="directors-name">Chandra Ming</span> <br>
                    <span class="directors-position">Founder & CEO</span> <br>
                </div>
                <div class="col-4 directors-wrapper">
                    <img class="directors-image" src="{{ asset('/assets/images/Mask Group-1 1.svg') }}" alt=""> <br>
                    <span class="directors-name">Juliana Siburian</span> <br>
                    <span class="directors-position">Co-Founder & COO</span> <br>
                </div>
                <div class="col-4 directors-wrapper">
                    <img class="directors-image" src="{{ asset('/assets/images/Mask Group 1.svg') }}" alt=""> <br>
                    <span class="directors-name">Alankar Joshi</span> <br>
                    <span class="directors-position">Co-Founder & CFO</span> <br>
                </div>
            </div>
        </div>
    </section>

    {{-- Section Footer --}}
    <section class="footer">
        <div class="footer-container">
            <div class="row">
                <div class="col-12">
                    <img class="footer-logo" src="{{ asset('/assets/images/jobseeker_white.png') }}" alt="" width="78" height="62">
                </div>
                <div class="col-8">
                    <p class="footer-address">
                        <b class="text-white">SINGAPORE</b> <br>
                        10 Anson Road #22-02, International Plaza, Singapore, 079903
                        
                        <br>
                        
                        <span style="float: right;">jobseeker.company</span>

                        <br>

                        <b class="text-white">INDONESIA</b> <br>
                        AD Premier Office Park, 9th Floor <br>
                        Jl. TB Simatupang No.5, Ragunan, Pasar Minggu <br>
                        South Jakarta City, Jakarta 12550 <br>
                    </p>

                    <a class="footer-email" href="mailto:info@jobseeker.company">
                        <i class="fa-regular fa-envelope" style="margin-right: 1rem;"></i> info@jobseeker.company
                    </a>

                    <br>

                    <a class="footer-email" href="tel:081318817887">
                        <i class="fa-brands fa-whatsapp" style="margin-right: 1rem;"></i> +62813 1881 7887
                    </a>

                    <br> <br>

                    <div class="d-flex">
                        <a class="footer-socmed" href="#"><i class="fa-brands fa-facebook" style="margin-right: 1rem;"></i></a> 
                        <a class="footer-socmed" href="#"><i class="fa-brands fa-instagram" style="margin-right: 1rem;"></i></a> 
                        <a class="footer-socmed" href="#"><i class="fa-brands fa-linkedin" style="margin-right: 1rem;"></i></a> 
                        <a class="footer-socmed" href="#"><i class="fa-brands fa-tiktok" style="margin-right: 1rem;"></i></a> 
                        <a class="footer-socmed" href="#"><i class="fa-brands fa-youtube" style="margin-right: 1rem;"></i></a> 
                        <a class="footer-socmed" href="#"><i class="fa-brands fa-telegram" style="margin-right: 1rem;"></i></a> 
                    </div>
                </div>
                <div class="col-2">
                    <p class="text-white footer-address">
                        <b>FOR EMPLOYER</b>

                        <a class="footer-app-url disabled" href="#"> jobseeker.partners</a> <br>
                        <span  class="footer-app-coming-soon">(Coming soon)</span> <br><br>

                        <a class="footer-app-url" href="#"> jobseeker.services</a> <br><br>

                        <a class="footer-app-url" href="#"> jobseeker.software</a>
                    </p>
                </div>
                <div class="col-2">
                    <p class="text-white footer-address">
                        <b>FOR CANDIDATE</b>

                        <a class="footer-app-url disabled" href="#"> jobseeker.app</a> <br>
                        <span class="footer-app-coming-soon">(Coming soon)</span> <br><br>
                    </p>
                </div>
            </div>

            <hr class="footer-divider">

            <div class="row">
                <div class="col-6">
                    <span class="copyright">Copyright © 2022 JobSeekerApps Pte. Ltd</span>
                </div>
                <div class="col-6">
                    <ul class="d-flex" style="float: right;">
                        <li><a class="footer-link" href="#">Privacy Policy</a></li>
                        <li><a class="footer-link" href="#">Terms of Service</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </section>

    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>